import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

import { Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
export class LocationsProvider {

  data: any;
  userLgn: number;
  userLat: number;
  usersLocation: any;

  constructor(public http: Http, public geolocation: Geolocation, public storage: Storage, private platform: Platform, ) { }

  load() {

    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http.get('assets/data/locations.json').map(res => res.json()).subscribe(data => {

        this.data = this.applyHaversine(data.locations);

        this.data.sort((locationA, locationB) => {
          return locationA.distance - locationB.distance;
        });

        resolve(this.data);
      });

    });

  }

  applyHaversine(locations) {

    this.platform.ready().then(() => {
      // get current position
      this.geolocation.getCurrentPosition().then(pos => {
        this.setLat(pos.coords.latitude);
        this.setLgn(pos.coords.longitude);

        let usersLocation = {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude
        };

        locations.map((location) => {

          let placeLocation = {
            lat: location.address.latitude,
            lng: location.address.longitude
          };

          location.distance = this.getDistanceBetweenPoints(
            usersLocation,
            placeLocation,
            'km'
          ).toFixed(2);
        });

        return locations;
      });

      const watch = this.geolocation.watchPosition().subscribe(pos => {
        this.setLat(pos.coords.latitude);
        this.setLgn(pos.coords.longitude);

        let usersLocation = {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude
        };

        locations.map((location) => {

          let placeLocation = {
            lat: location.address.latitude,
            lng: location.address.longitude
          };

          location.distance = this.getDistanceBetweenPoints(
            usersLocation,
            placeLocation,
            'km'
          ).toFixed(2);
        });

        return locations;
      });

      // to stop watching
      watch.unsubscribe();
    });

    return locations;
  }

  getDistanceBetweenPoints(start, end, units) {

    let earthRadius = {
      miles: 3958.8,
      km: 6371
    };

    let R = earthRadius[units || 'km'];
    let lat1 = start.lat;
    let lon1 = start.lng;
    let lat2 = end.lat;
    let lon2 = end.lng;

    let dLat = this.toRad((lat2 - lat1));
    let dLon = this.toRad((lon2 - lon1));
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;

    return d;

  }

  toRad(x) {
    return x * Math.PI / 180;
  }

  public getLat() {
    return this.userLat;
  }

  setLat(lat: number) {
    this.userLat = lat;
  }
  public getLgn() {
    return this.userLgn;
  }

  setLgn(lgn: number) {
    this.userLgn = lgn;
  }
}

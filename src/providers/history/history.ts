import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HistoryProvider {

  history: Array<any> = [];
  rated: boolean;

  constructor(public http: Http) { }

  addHistory(attendance){
    this.rated = false;
    this.history.push(attendance);
  }

  rateAttendance(attendance){
    this.history.pop();
    this.history.push(attendance);
    this.rated = true;
  }

  listHistory(){
    return this.history;
  }

}

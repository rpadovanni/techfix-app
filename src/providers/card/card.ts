import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CardProvider {

  cards: Array<any> = [];

  constructor(public http: Http) { }

  addCard(card){
    console.log("ADD CARD PROVIDER");
    this.cards.push(card);
    console.log(this.cards);
  }

  removeCard(index){
    this.cards.splice(index, 1);
  }

  listCard(){
    return this.cards;
  }
  
}

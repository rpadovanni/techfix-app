import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
//import { SettingsPage } from '../pages/settings/settings';
import { PaymentMethodsPage } from '../pages/payment-methods/payment-methods';
import { HistoryPage } from '../pages/history/history';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = LoginPage;

  pages: Array<{ title: string, component: any }>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.pages = [
      { title: 'Tela Inicial - Mapa', component: HomePage },
      { title: 'Formas de Pagamento', component: PaymentMethodsPage },
      { title: 'Atendimentos', component: HistoryPage },
      //{ title: 'Configurações', component: SettingsPage }
    ];
  }

  logout(){
    this.storage.set('logged', false);
    this.nav.popTo(LoginPage);
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

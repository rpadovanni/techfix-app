import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { SettingsPage } from '../pages/settings/settings';
import { HomePage } from '../pages/home/home';
import { DirectionPage } from '../pages/direction/direction';
import { PaymentMethodsPage } from '../pages/payment-methods/payment-methods';
import { HistoryPage } from '../pages/history/history';
import { ServiceRequestPage } from '../pages/service-request/service-request'
import { TechnicianPage } from '../pages/technician/technician';
import { PaymentPage } from '../pages/payment/payment';
import { RatingPage } from '../pages/rating/rating';
import { WorkPage } from '../pages/work/work';
import { PaymentRegisterPage } from '../pages/payment-register/payment-register';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';

import { LocationsProvider } from '../providers/locations/locations';

import { ObjectValuesPipe } from '../pipes/object-values/object-values';
import { CardProvider } from '../providers/card/card';
import { HistoryProvider } from '../providers/history/history';


@NgModule({
  declarations: [
    MyApp,
    SettingsPage,
    HomePage,
    DirectionPage,
    PaymentMethodsPage,
    HistoryPage,
    ServiceRequestPage,
    TechnicianPage,
    PaymentPage,
    RatingPage,
    WorkPage,
    PaymentRegisterPage,
    LoginPage,
    RegisterPage,
    ObjectValuesPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__bhpdb',
         driverOrder: ['sqlite', 'indexeddb', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SettingsPage,
    HomePage,
    DirectionPage,
    PaymentMethodsPage,
    HistoryPage,
    ServiceRequestPage,
    TechnicianPage,
    PaymentPage,
    RatingPage,
    WorkPage,
    LoginPage,
    PaymentRegisterPage,
    RegisterPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LocationsProvider,
    ObjectValuesPipe,
    CardProvider,
    HistoryProvider,
    Camera
  ]
})
export class AppModule {}

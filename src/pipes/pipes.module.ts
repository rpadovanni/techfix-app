import { NgModule } from '@angular/core';
import { ObjectValuesPipe } from './object-values/object-values';
@NgModule({
	declarations: [ObjectValuesPipe],
	imports: [],
	exports: [ObjectValuesPipe]
})
export class PipesModule {}

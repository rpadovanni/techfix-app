import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  private name: string;
  private cpf: string;
  private email: string;
  private password: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    private alertCtrl: AlertController
  ) { }

  register(){
    let newUser = {
      name: this.name,
      cpf: this.cpf,
      email: this.email,
      password: this.password
    }
    this.storage.set(newUser.email + newUser.password, newUser).then(suc => {
      let alert = this.alertCtrl.create({
        title: 'Sucesso!',
        message: 'Cadastro realizado com sucesso.',
        buttons: ['OK']
      });
      alert.present();
      this.closeView();
    },
      err => {
        let alert = this.alertCtrl.create({
          title: 'Ops!',
          message: 'Algo deu errado enquanto criávamos sua conta. Tente novamente mais tarde.',
          buttons: ['OK']
        });
        alert.present();
      });
  }

  closeView(){
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CardProvider } from '../../providers/card/card';

import { PaymentRegisterPage } from '../payment-register/payment-register';

@IonicPage()
@Component({
  selector: 'page-payment-methods',
  templateUrl: 'payment-methods.html',
})
export class PaymentMethodsPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public cards: CardProvider
  ) { }

  removeCard(index){
    this.cards.removeCard(index);
  }

  openPaymentRegisterPage(){
    this.navCtrl.push(PaymentRegisterPage);
  }

}

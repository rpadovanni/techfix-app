import { Component } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController, IonicPage } from 'ionic-angular';
import { LocationsProvider } from '../../providers/locations/locations';

import { WorkPage } from '../work/work';

declare var google;

@IonicPage()
@Component({
  selector: 'page-technician',
  templateUrl: 'technician.html',
})
export class TechnicianPage {

  title: string;
  mapD: any;
  infoWindow: any;
  markers: any = [];
  requestInfo: any = [];
  destination: any;
  myLocation: any;
  count: number = 0;
  cancelCheck: boolean = false;
  cancelCheck2: boolean = false;
  cancelButton2: boolean = false;

  constructor(public navCtrl: NavController,
    public platform: Platform,
    public locations: LocationsProvider,
    public navParams: NavParams,
    private _loadingCtrl: LoadingController
  ) {
    this.requestInfo = this.navParams.get('requestInfo');
  }

  showMyLocation() {
    this.traceRoute();
  }

  traceRoute() {
    let that = this;
    let loader = this._loadingCtrl.create({
      content: 'Calculando rota ...'
    });

    loader.present();

    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;
    const mapD = new google.maps.Map(document.getElementById('mapD'), {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 16,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: false,
      fullscreenControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    directionsDisplay.setMap(mapD);

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        mapD.setCenter(pos);
        let origin = new google.maps.LatLng(pos);
        let destination = new google.maps.LatLng(-23.492255, -46.466051);

        console.log("Antes Direction Origem:" + origin);
        console.log("Antes Direction Destino:" + destination);

        directionsService.route({
          origin: origin,
          destination: destination,
          travelMode: 'DRIVING'
        }, function (response, status) {
          if (status === 'OK') {
            loader.dismiss();
            directionsDisplay.setDirections(response);
          } else {
            loader.dismiss();
            window.alert('Directions request failed due to ' + status);
          }
        });

      }, function () {
        this.handleLocationError(true, that.infoWindow, that.mapD.getCenter(), that.mapD);
      });
    } else {
      // Browser doesn't support Geolocation
    }
  }

  cancelRequest() {
    this.cancelCheck = true;
    this.navCtrl.popToRoot();
  }

  cancelRequest2() {
    this.cancelCheck2 = true;
    this.navCtrl.popToRoot();
  }

  nextStep(){
    this.navCtrl.push(WorkPage, { requestInfo: this.requestInfo });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TechnicianPage');
  }

  ionViewDidEnter() {
    let that = this;

    this.title = "PROCURANDO TÉCNICO";

    console.log(that.cancelCheck);

    setTimeout(function () {
      if (!that.cancelCheck) {
        console.log("ENTROU NO IF");
        setTimeout(function () {
          that.cancelButton2 = true;
          that.title = "TÉCNICO À CAMINHO";
          that.traceRoute();
        }, 3000)
      }
    }, 3000)
  }

}

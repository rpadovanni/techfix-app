import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  maxDistance: number;

  constructor(public navCtrl: NavController, public storage: Storage) {
    // Get actual maxDistance
    this.storage.get('maxDistance').then((data) => {
      this.maxDistance = data;
    });
  }

  setSettings(val) {
    console.log('Data Added' + val);
    this.storage.set('maxDistance', val);
  }

}

import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';

import { LocationsProvider } from '../../providers/locations/locations';
import { CardProvider } from '../../providers/card/card';
import { HistoryProvider } from '../../providers/history/history';

import { ServiceRequestPage } from '../service-request/service-request';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  map: any;
  infoWindow: any;
  markers: any = [];
  destination: any;
  myLocation: any;
  count: number = 1;
  loaderGeral: any;
  isLoaded: boolean = false;

  constructor(public navCtrl: NavController,
    public platform: Platform,
    public locations: LocationsProvider,
    public navParams: NavParams,
    public storage: Storage,
    public cards: CardProvider,
    private _loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public hist: HistoryProvider
  ) {
  }

  showMyLocation() {
    this.googleMap();
  }

  googleMap() {
    let that = this;

    this.loaderGeral.dismiss();

    let loader = this._loadingCtrl.create({
      content: 'Carregando mapa. Aguarde ...'
    });

    loader.present();

    // HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var iconBase = 'assets/icon/';
        that.map = new google.maps.Map(document.getElementById('map'), {
          center: latLng,
          zoom: 16,
          zoomControl: false,
          mapTypeControl: false,
          scaleControl: true,
          streetViewControl: false,
          rotateControl: false,
          fullscreenControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
        });

        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        var marker = new google.maps.Marker({
          map: that.map,
          animation: google.maps.Animation.DROP,
          position: latLng,
          icon: iconBase + 'user_pointer_g.png'
        });
        that.map.setCenter(pos);
        that.myLocation = new google.maps.LatLng(pos);

        that.locations.load().then((result) => {
          let locations = result;

          for (let location of locations) {
            let placeL = new google.maps.LatLng(location.address.latitude, location.address.longitude);
            let distance = google.maps.geometry.spherical.computeDistanceBetween(that.myLocation, placeL);
            distance = (distance / 1000).toFixed(2);

            that.addMarker(location.address.latitude, location.address.longitude, location);

          }
        });
        loader.dismiss();
        that.isLoaded = true;
      }, function () {
        this.handleLocationError(true, that.infoWindow, that.map.getCenter(), that.map);
      });
    } else {
      // Browser doesn't support Geolocation
      this.handleLocationError(false, that.infoWindow, that.map.getCenter(), that.map);
    }
  }

  handleLocationError(browserHasGeolocation, infoWindow, pos, map) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
      'Error: The Geolocation service failed.' :
      'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
  }

  addMarker(lat: number, lng: number, location: any): void {
    let latLng = new google.maps.LatLng(lat, lng);

    var iconBase = 'assets/icon/';
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      icon: iconBase + 'car_pointer.png'
    });

    this.markers.push(marker);
  }

  callServiceRequestPage() {
    let registeredCards = this.cards.listCard();
    let registeredHistory = this.hist.rated;

    if (registeredCards.length <= 0) {
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        message: 'Por favor, cadastre um cartão de crédito para abrir um chamado.',
        buttons: ['OK']
      });
      alert.present();
    } else if (registeredHistory == false){
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        message: 'Por favor, avalie o último atendimento para abrir outro chamado.',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.navCtrl.push(ServiceRequestPage);
    }
  }

  ionViewDidEnter() {
    console.log('ENTERED');
  }

  ionViewDidLoad() {
    const that = this;

    this.loaderGeral = this._loadingCtrl.create({
      content: 'Entrando no mapa ...'
    });

    this.loaderGeral.present();

    setTimeout(function () {
      that.showMyLocation();
    }, 2000)

  }

}

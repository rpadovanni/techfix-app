import { Component } from '@angular/core';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { TechnicianPage } from '../technician/technician';

@IonicPage()
@Component({
  selector: 'page-service-request',
  templateUrl: 'service-request.html',
})
export class ServiceRequestPage {

  problemType: string;
  brokenDevice: string;
  details: string;
  base64Image: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public camera: Camera
  ) {
    this.problemType = "none";
    this.brokenDevice = "none";
  }

  cancelRequest() {
    this.navCtrl.popToRoot()
  }

  openRequest() {
    if (this.problemType == "none") {
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        message: 'Por favor, preencha todos os campos.',
        buttons: ['OK']
      });
      alert.present();
    } else if (typeof this.details == "undefined") {
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        message: 'Por favor, preencha todos os campos.',
        buttons: ['OK']
      });
      alert.present();
    } else {
      var requestInfo = {
        problemType: this.problemType,
        brokenDevice: this.brokenDevice,
        details: this.details
      }
      console.log(requestInfo);
      this.navCtrl.push(TechnicianPage, { requestInfo: requestInfo });
    }

  }

  accessGallery() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL
    }).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServiceRequestPage');
  }

}

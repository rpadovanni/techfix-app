import { Component } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController } from 'ionic-angular';
import { LocationsProvider } from '../../providers/locations/locations';

declare var google;

@Component({
  selector: 'page-direction',
  templateUrl: 'direction.html',
})
export class DirectionPage {

  mapD: any;
  infoWindow: any;
  markers: any = [];
  destination: any;
  myLocation: any;
  count: number = 0;

  constructor(public navCtrl: NavController,
    public platform: Platform,
    public locations: LocationsProvider,
    public navParams: NavParams,
    private _loadingCtrl: LoadingController
  ) {
    this.destination = this.navParams.get('restaurantAddress');
    this.destination = new google.maps.LatLng(this.destination.lat, this.destination.lng);
    console.log("Constuctor: " + this.destination);
  }

  showMyLocation() {
    this.traceRoute();
  }

  traceRoute() {
    let that = this;
    let loader = this._loadingCtrl.create({
      content: 'Calculando rota ...'
    });
    
    loader.present();

    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;
    const mapD = new google.maps.Map(document.getElementById('mapD'), {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 16,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: false,
      fullscreenControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    directionsDisplay.setMap(mapD);

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        mapD.setCenter(pos);
        let origin = new google.maps.LatLng(pos);

        console.log("Antes Direction Origem:" + origin);
        console.log("Antes Direction Destino:" + that.destination);

        directionsService.route({
          origin: origin,
          destination: that.destination,
          travelMode: 'DRIVING'
        }, function (response, status) {
          if (status === 'OK') {
            loader.dismiss();
            directionsDisplay.setDirections(response);
          } else {
            loader.dismiss();
            window.alert('Directions request failed due to ' + status);
          }
        });

      }, function () {
        this.handleLocationError(true, that.infoWindow, that.mapD.getCenter(), that.mapD);
      });
    } else {
      // Browser doesn't support Geolocation
    }

    



    /*let that = this;
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;

    
    //that.getCurrentLocation();
    that.googleMapD();
    directionsDisplay.setMap(that.mapD);

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        that.mapD.setCenter(pos);
        that.myLocation = new google.maps.LatLng(pos);

      }, function () {

      });
    } else {
      // Browser doesn't support Geolocation
    }

    directionsService.route(
      {
        origin: that.myLocation,
        destination: that.destination,
        provideRouteAlternatives: false,
        travelMode: 'DRIVING',
        drivingOptions: {
          departureTime: new Date(),
          trafficModel: 'pessimistic'
        },
        unitSystem: google.maps.UnitSystem.IMPERIAL
      }, function (response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });*/

  }

  ionViewDidEnter() {
    this.traceRoute();
  }
}

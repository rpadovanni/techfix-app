import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string;
  password: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, private alertCtrl: AlertController) {
    this.storage.get('logged').then((data) => {
      if (data == true) {
        this.navCtrl.push(HomePage);
      }
    })
  }

  login() {
    this.storage.get(this.email + this.password).then((data) => {
      if (data == null) {
        let alert = this.alertCtrl.create({
          title: 'Ops!',
          message: 'Usuário não encontrado. Verifique seus dados e tente novamente.',
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.storage.set('logged', true);
        this.navCtrl.push(HomePage);
      }
    })
  }

  createAccount() {
    this.navCtrl.push(RegisterPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { PaymentPage } from '../payment/payment';

@IonicPage()
@Component({
  selector: 'page-work',
  templateUrl: 'work.html',
})
export class WorkPage {

  requestInfo: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.requestInfo = this.navParams.get('requestInfo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkPage');
  }

  ionViewDidEnter(){
    let that = this;

    setTimeout(function () {
      that.navCtrl.push(PaymentPage, { requestInfo: that.requestInfo });
    }, 6000)
  }

}

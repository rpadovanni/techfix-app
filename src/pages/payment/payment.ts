import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CardProvider } from '../../providers/card/card';
import { HistoryProvider } from '../../providers/history/history';

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  paymentOk: boolean = false;
  history: any;
  count: number = 0;
  requestInfo: any = [];
  sc: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public cards: CardProvider,
    public hist: HistoryProvider
  ) {
      this.requestInfo = this.navParams.get('requestInfo');
      this.requestInfo.rating = null;
  }

  setPaymentOk(){
    this.paymentOk = true;
  }

  endRequest(){
    this.createHistory();
    this.navCtrl.popToRoot();
    this.storage.set('rated', "not rated");
  }

  createHistory() {
    this.hist.addHistory(this.requestInfo);
    /*let val = [];
    val.push(this.requestInfo);
    console.log(val);
    this.storage.get('history').then((data) => {
      if (data != null) {
        data.push(val);
        this.storage.set('history', data);   
      } else {
        let array = [];
        array.push(val);
        this.storage.set('history', array);
      }
    })*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

}

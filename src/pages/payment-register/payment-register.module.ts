import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentRegisterPage } from './payment-register';

@NgModule({
  declarations: [
    PaymentRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentRegisterPage),
  ],
})
export class PaymentRegisterPageModule {}

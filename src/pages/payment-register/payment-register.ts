import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { CardProvider } from '../../providers/card/card';

@IonicPage()
@Component({
  selector: 'page-payment-register',
  templateUrl: 'payment-register.html',
})
export class PaymentRegisterPage {

  creditCard = {
    holder: '',
    num: '',
    cvv: '',
    monthExp: '',
    yearExp: '',
    brand: '',
    token: ''
  };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public cards: CardProvider
  ) { }

  addCard(){
    console.log(this.creditCard);
    this.cards.addCard(this.creditCard);
    let toast = this.toastCtrl.create({
      message: 'Cartão salvo com sucesso.',
      duration: 2000
    })
    toast.present();
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentRegisterPage');
  }

}

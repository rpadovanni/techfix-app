import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HistoryProvider } from '../../providers/history/history';

import { RatingPage } from '../rating/rating'

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  history: any;
  message: string;
  count: number = 0;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public hist: HistoryProvider
  ) {
    this.storage.clear().then(() => {
      console.log('Key Cleared');
    });
  }

  openRatingPage(attendance) {
    attendance.advice = "Problema resolvido de X forma."
    this.navCtrl.push(RatingPage, { attendance: attendance });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
    // Verifica se existe historico de atendimentos
    if (this.hist.listHistory().length <= 0) {
      this.message = "Não há um histórico de atendimentos ainda."
    } else {
      this.history = this.hist.listHistory();
    }
    /*this.storage.get('history').then((data) => {
      if (data == null) {
        this.message = "Não há um histórico de atendimentos ainda."
      } else {
        this.history = data;
      }
    })*/
  }

}

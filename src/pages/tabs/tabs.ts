import { Component } from '@angular/core';

import { SettingsPage } from '../settings/settings';
import { HomePage } from '../home/home';
import { DirectionPage } from '../direction/direction';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab3Root = SettingsPage;
  tab6Root = DirectionPage;

  constructor() {

  }
}

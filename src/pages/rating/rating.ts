import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { HistoryProvider } from '../../providers/history/history';

@IonicPage()
@Component({
  selector: 'page-rating',
  templateUrl: 'rating.html',
})
export class RatingPage {

  attendance: any;
  rate: any;
  ratingDetails: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    private alertCtrl: AlertController,
    public hist: HistoryProvider
  ) {
    this.attendance = this.navParams.get('attendance');
    this.rate = 0;
  }

  closeView(){
    this.navCtrl.pop();
  }

  saveRating() {
    this.attendance.rating = this.rate;
    this.attendance.ratingDetails = this.ratingDetails;
    this.attendance.rated = true;
    this.hist.rateAttendance(this.attendance);
    
    /*this.storage.set('rated', 'rated').then(suc => {
      this.attendance.rating = 5;

      let alert = this.alertCtrl.create({
        title: 'Sucesso!',
        message: 'Avaliação salva com sucesso.',
        buttons: ['OK']
      });
      alert.present();
    },
      err => {
        let alert = this.alertCtrl.create({
          title: 'Ops!',
          message: 'Algo deu errado na avaliação.',
          buttons: ['OK']
        });
        alert.present();
      });*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RatingPage');
  }

}
